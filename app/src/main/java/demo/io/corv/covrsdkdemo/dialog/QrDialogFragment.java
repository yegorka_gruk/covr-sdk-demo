package demo.io.corv.covrsdkdemo.dialog;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.dlazaro66.qrcodereaderview.QRCodeReaderView;

public class QrDialogFragment extends DialogFragment {

    private static QrDialogFragment instance;

    public static QrDialogFragment getInstance() {
        if (instance == null) {
            instance = new QrDialogFragment();
        }
        return instance;
    }

    private QRCodeReaderView mQrCodeReaderView;
    private QRCodeReaderView.OnQRCodeReadListener mScanListener;

    public void setScanListener(@NonNull QRCodeReaderView.OnQRCodeReadListener scanListener) {
        this.mScanListener = scanListener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        mQrCodeReaderView = new QRCodeReaderView(getActivity());
        mQrCodeReaderView.setOnQRCodeReadListener(mScanListener);
        mQrCodeReaderView.setQRDecodingEnabled(true);
        mQrCodeReaderView.setAutofocusInterval(2000L);
        mQrCodeReaderView.setBackCamera();
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return mQrCodeReaderView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mQrCodeReaderView.startCamera();
    }

    @Override
    public void onStop() {
        super.onStop();
        mQrCodeReaderView.stopCamera();
    }
}
