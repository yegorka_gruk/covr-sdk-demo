package demo.io.corv.covrsdkdemo.fragment;

import android.app.Fragment;
import android.os.Bundle;

import demo.io.corv.covrsdkdemo.FragmentAnimationSet;

public interface IFragmentListener {

    void replaceFragment(Fragment fragment, Bundle bundle, boolean addToBackStack);

    void replaceFragment(Fragment fragment, Bundle bundle, boolean addToBackStack, int containerViewId,
                         int showAnimationId, int hideAnimationId);

    void replaceFragment(Fragment fragment, Bundle bundle, boolean addToBackStack, FragmentAnimationSet transitionAnimations);

    void showProgress();

    void hideProgress();

//    void onNoInternetReceived(NoInternetEvent event);
}
