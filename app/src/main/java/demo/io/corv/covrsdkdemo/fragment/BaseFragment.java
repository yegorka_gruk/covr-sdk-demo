package demo.io.corv.covrsdkdemo.fragment;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import demo.io.corv.covrsdkdemo.FragmentAnimationSet;
import demo.io.corv.covrsdkdemo.activity.BaseActivity;

public abstract class BaseFragment<Binding extends ViewDataBinding> extends Fragment {

    protected Binding mBinding;
    protected IFragmentListener fragmentListener;

    @Deprecated
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        initData(savedInstanceState);
        initTools();
        initBinding(inflater);
        return mBinding.getRoot();
    }

    protected void initBinding(LayoutInflater inflater) {
        mBinding = DataBindingUtil.inflate(inflater, getLayoutId(), null, false);
    }

    @CallSuper
    protected void initData(Bundle savedInstanceState) {
    }

    @CallSuper
    protected void initTools() {
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            onAttachActivity((BaseActivity) activity);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        onAttachActivity((BaseActivity) context);
    }

    protected void onAttachActivity(BaseActivity activity) {
        try {
            fragmentListener = activity;
        } catch (ClassCastException classCastException) {
            throw new IllegalAccessError("Activity MUST implement IFragmentListener");
        }
    }

    protected void hideProgress() {
        fragmentListener.hideProgress();
    }

    protected void showProgress() {
        fragmentListener.showProgress();
    }

//    public void onLocked() {
//    }

    protected void replaceFragment(Fragment fragment, Bundle bundle, boolean addToBackStack) {
        fragmentListener.replaceFragment(fragment, bundle, addToBackStack);
    }

    protected void replaceFragment(Fragment fragment, Bundle bundle, boolean addToBackStack, int containerViewId,
                                   int showAnimationId, int hideAnimationId) {
        fragmentListener.replaceFragment(fragment, bundle, addToBackStack, containerViewId, showAnimationId, hideAnimationId);
    }

    protected void replaceFragment(Fragment fragment, Bundle bundle, boolean addToBackStack,
                                   FragmentAnimationSet transitionAnimations) {
        fragmentListener.replaceFragment(fragment, bundle, addToBackStack, transitionAnimations);
    }

//    protected void showNoInternet() {
//        fragmentListener.onNoInternetReceived(new NoInternetEvent());
//    }

    protected abstract int getLayoutId();

}
