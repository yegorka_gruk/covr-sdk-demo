package demo.io.corv.covrsdkdemo;

public class FragmentAnimationSet {

    public static final FragmentAnimationSet FADE_IN = new FragmentAnimationSet(R.animator.fade_in, R.animator.fade_out, R.animator.fade_in, R.animator.fade_out);

    public int enter;
    public int exit;
    public int popEnter;
    public int popExit;

    public FragmentAnimationSet(int enter, int exit, int popEnter, int popExit) {
        this.enter = enter;
        this.exit = exit;
        this.popEnter = popEnter;
        this.popExit = popExit;
    }
}
