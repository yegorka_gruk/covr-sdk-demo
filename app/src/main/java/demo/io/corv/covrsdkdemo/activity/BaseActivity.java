package demo.io.corv.covrsdkdemo.activity;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import demo.io.corv.covrsdkdemo.FragmentAnimationSet;
import demo.io.corv.covrsdkdemo.dialog.ProgressDialogFragment;
import demo.io.corv.covrsdkdemo.fragment.IFragmentListener;
import timber.log.Timber;

public abstract class BaseActivity extends AppCompatActivity implements IFragmentListener {

    private ProgressDialogFragment mProgressDialog;

    @Override
    public void replaceFragment(Fragment fragment, Bundle bundle,
                                boolean addToBackStack) {
        replaceFragment(fragment, bundle, addToBackStack, getFragmentContainerId(), 0, 0);
    }

    @Override
    public void replaceFragment(Fragment fragment, Bundle bundle, boolean addToBackStack,
                                FragmentAnimationSet animations) {
        replaceFragment(fragment, bundle, addToBackStack, getFragmentContainerId(),
                animations.enter, animations.exit, animations.popEnter, animations.popExit);
    }

    public void replaceFragment(Fragment fragment, Bundle bundle,
                                boolean addToBackStack, int containerViewId,
                                int showAnimationId, int hideAnimationId) {
        replaceFragment(fragment, bundle, addToBackStack, getFragmentContainerId(), showAnimationId, 0, 0, hideAnimationId);
    }

    public void replaceFragment(Fragment fragment, Bundle bundle,
                                boolean addToBackStack, int containerViewId,
                                int enterAnimationId, int exitAnimationId,
                                int popEnterAnimationId, int popExitAnimationId) {
        String fragmentName = fragment.getClass().getName();
        Timber.d("Replacing fragment with %s", fragmentName);
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        if (addToBackStack) {
            ft.addToBackStack(fragmentName);
        }
        Fragment f = Fragment.instantiate(BaseActivity.this, fragmentName, bundle);
        if (enterAnimationId != 0 && popExitAnimationId != 0) {
            ft.setCustomAnimations(enterAnimationId, exitAnimationId, popEnterAnimationId, popExitAnimationId);
        }
        ft.replace(containerViewId, f, fragmentName);
        try {
            ft.commitAllowingStateLoss();
        } catch (IllegalStateException e) {
            Timber.e(e, "Error replacing fragment");
        }
    }

    protected final Fragment getCurrentFragment() {
        return getFragmentManager().findFragmentById(getFragmentContainerId());
    }

    @Override
    public void showProgress() {
        hideProgress();
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialogFragment();
        }
        FragmentManager fm = getFragmentManager();
        Fragment fragment = fm.findFragmentByTag(ProgressDialogFragment.class.getSimpleName());
        FragmentTransaction ft = fm.beginTransaction();
        if (fragment != null) {
            ft.remove(fragment);
        }
        mProgressDialog = new ProgressDialogFragment();
        ft.add(mProgressDialog, ProgressDialogFragment.class.getSimpleName());
        ft.commitAllowingStateLoss();
    }

    @Override
    public void hideProgress() {
        if (mProgressDialog != null) {
            mProgressDialog.dismissAllowingStateLoss();
        }
    }

    protected abstract int getFragmentContainerId();
}
