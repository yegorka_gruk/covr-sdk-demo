package demo.io.corv.covrsdkdemo.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;

import demo.io.corv.covrsdkdemo.R;
import demo.io.corv.covrsdkdemo.databinding.FragmentDefaultBinding;

public class DefaultFragment extends BaseFragment<FragmentDefaultBinding> {

    private static final String TEXT_EXTRA = "TEXT_EXTRA";

    public static DefaultFragment getInstance(String text) {
        Bundle args = new Bundle();
        args.putString(TEXT_EXTRA, text);
        DefaultFragment fragment = new DefaultFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private String mText;

    @Override
    protected void initData(Bundle savedInstanceState) {
        super.initData(savedInstanceState);
        mText = getArguments().getString(TEXT_EXTRA);
    }

    @Override
    protected void initBinding(LayoutInflater inflater) {
        super.initBinding(inflater);
        mBinding.setText(mText);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_default;
    }
}
