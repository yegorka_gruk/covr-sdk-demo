package demo.io.corv.covrsdkdemo.fragment.covr;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.Toast;

import demo.io.corv.covrsdkdemo.FragmentAnimationSet;
import demo.io.corv.covrsdkdemo.R;
import demo.io.corv.covrsdkdemo.SdkTestApp;
import demo.io.corv.covrsdkdemo.databinding.FragmentCovrLockScreenBinding;
import demo.io.corv.covrsdkdemo.fragment.BaseFragment;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class CovrLockScreenFragment extends BaseFragment<FragmentCovrLockScreenBinding> {

    public static CovrLockScreenFragment getInstance() {
        return new CovrLockScreenFragment();
    }

    @Override
    protected void initData(Bundle savedInstanceState) {
        super.initData(savedInstanceState);
    }

    @Override
    protected void initBinding(LayoutInflater inflater) {
        super.initBinding(inflater);
        mBinding.confirmPersonalCode.setOnClickListener(v -> {
            Single.fromCallable(() -> {
                char[] covrCode = mBinding.personalCodeEdit.getText().toString().toCharArray();
                return SdkTestApp.getsSdkTestApp().getCovrSdkMainClass().chechCovrCode(getActivity(), covrCode);
            })
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(aBoolean -> {
                                Log.w("CovrLockScreenFragment", "chechCovrCode: " + aBoolean);
                                if (aBoolean) {
                                    CovrMainScreenFragment fragment = CovrMainScreenFragment.getInstance();
                                    replaceFragment(fragment, fragment.getArguments(), true, FragmentAnimationSet.FADE_IN);
                                } else {
                                    Toast.makeText(getActivity(), "not match ", Toast.LENGTH_SHORT).show();
                                }
                            },
                            throwable -> {
                                Log.e("CovrLockScreenFragment", "chechCovrCode: " + throwable.getMessage());
                                Toast.makeText(getActivity(), "chechCovrCode " + throwable.getMessage(), Toast.LENGTH_SHORT).show();
                            });
        });
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_covr_lock_screen;
    }
}
