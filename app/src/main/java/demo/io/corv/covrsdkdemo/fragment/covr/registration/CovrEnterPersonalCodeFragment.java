package demo.io.corv.covrsdkdemo.fragment.covr.registration;

import android.app.Fragment;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.Toast;

import com.covrsecurity.io.network.base.IResponseListener;
import com.covrsecurity.io.network.response.Error;
import com.covrsecurity.io.sdk.model.request.RegisterRequestSdk;
import com.covrsecurity.io.sdk.model.response.RegisterResponseSdk;

import java.util.List;

import demo.io.corv.covrsdkdemo.FragmentAnimationSet;
import demo.io.corv.covrsdkdemo.R;
import demo.io.corv.covrsdkdemo.SdkTestApp;
import demo.io.corv.covrsdkdemo.databinding.FragmentCovrEnterPersonalCodeBinding;
import demo.io.corv.covrsdkdemo.fragment.BaseFragment;
import demo.io.corv.covrsdkdemo.fragment.covr.CovrMainScreenFragment;

public class CovrEnterPersonalCodeFragment extends BaseFragment<FragmentCovrEnterPersonalCodeBinding> {

    public static final String TAG = "EnterPersonalCode";

    private static final String PHONE_EXTRA = "PHONE_EXTRA";

    public static CovrEnterPersonalCodeFragment getInstance(String phoneNumber) {
        Bundle args = new Bundle();
        args.putString(PHONE_EXTRA, phoneNumber);
        CovrEnterPersonalCodeFragment fragment = new CovrEnterPersonalCodeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private String mPhoneNumber;
    private String mCovrCode;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_covr_enter_personal_code;
    }

    @Override
    protected void initData(Bundle savedInstanceState) {
        super.initData(savedInstanceState);
        mPhoneNumber = getArguments().getString(PHONE_EXTRA);
    }

    @Override
    protected void initBinding(LayoutInflater inflater) {
        super.initBinding(inflater);
        mBinding.confirmPersonalCode.setOnClickListener(v -> {
            if (mBinding.personalCodeEdit.getText().length() < 6) {
                mBinding.personalCodeEditInputLayout.setError("Code should be 6 digits");
                return;
            }
            if (TextUtils.isEmpty(mCovrCode)) {
                mCovrCode = mBinding.personalCodeEdit.getText().toString();
                mBinding.personalCodeEdit.setText("");
                mBinding.personalCodeEditInputLayout.setHint("Reenter Covr code");
                return;
            } else if (!mCovrCode.equals(mBinding.personalCodeEdit.getText().toString())) {
                mBinding.personalCodeEditInputLayout.setError("Codes don't match");
                return;
            }
            showProgress();
            RegisterRequestSdk request = new RegisterRequestSdk(mPhoneNumber, mCovrCode.toCharArray());
            SdkTestApp.getsSdkTestApp().getRegistrationApiPublic().register(request).enqueue(new IResponseListener<RegisterResponseSdk>() {
                @Override
                public void onResponseReceived(RegisterResponseSdk response) {
                    Log.w(TAG, "" + response);
                    hideProgress();
                    getActivity().runOnUiThread(() -> Toast.makeText(getActivity(), "" + response, Toast.LENGTH_SHORT).show());
                    Fragment fragment = CovrMainScreenFragment.getInstance();
                    replaceFragment(fragment, fragment.getArguments(), true, FragmentAnimationSet.FADE_IN);
                }

                @Override
                public void onError(List<Error> errors) {
                    Log.e(TAG, "" + errors);
                    hideProgress();
                    getActivity().runOnUiThread(() -> Toast.makeText(getActivity(), "" + errors, Toast.LENGTH_SHORT).show());
                }
            });
        });
        mBinding.personalCodeEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                mBinding.personalCodeEditInputLayout.setError(null);
            }
        });
    }
}