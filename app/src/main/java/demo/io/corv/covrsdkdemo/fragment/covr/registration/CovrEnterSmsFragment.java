package demo.io.corv.covrsdkdemo.fragment.covr.registration;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.Toast;

import com.covrsecurity.io.network.base.IResponseListener;
import com.covrsecurity.io.network.response.Error;
import com.covrsecurity.io.sdk.model.request.CheckVerificationCodeRequestSdk;
import com.covrsecurity.io.sdk.model.response.CheckVerificationCodeResponseSdk;

import java.util.List;

import demo.io.corv.covrsdkdemo.FragmentAnimationSet;
import demo.io.corv.covrsdkdemo.R;
import demo.io.corv.covrsdkdemo.SdkTestApp;
import demo.io.corv.covrsdkdemo.databinding.FragmentCovrEnterSmsBinding;
import demo.io.corv.covrsdkdemo.fragment.BaseFragment;

public class CovrEnterSmsFragment extends BaseFragment<FragmentCovrEnterSmsBinding> {

    private static final String PHONE_EXTRA = "PHONE_EXTRA";

    public static CovrEnterSmsFragment getInstance(String phoneNumber) {
        Bundle args = new Bundle();
        args.putString(PHONE_EXTRA, phoneNumber);
        CovrEnterSmsFragment fragment = new CovrEnterSmsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private String mPhoneNumber;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_covr_enter_sms;
    }

    @Override
    protected void initData(Bundle savedInstanceState) {
        super.initData(savedInstanceState);
        mPhoneNumber = getArguments().getString(PHONE_EXTRA);
    }

    @Override
    protected void initBinding(LayoutInflater inflater) {
        super.initBinding(inflater);
        mBinding.verifySms.setOnClickListener(v -> {
            showProgress();
            CheckVerificationCodeRequestSdk request = new CheckVerificationCodeRequestSdk(mPhoneNumber, mBinding.smsEdit.getText().toString());
            SdkTestApp.getsSdkTestApp().getRegistrationApiPublic().checkVerificationCode(request).enqueue(new IResponseListener<CheckVerificationCodeResponseSdk>() {
                @Override
                public void onResponseReceived(CheckVerificationCodeResponseSdk checkVerificationCodeResponse) {
                    Log.w("CovrEnterSmsFragment", checkVerificationCodeResponse.toString());
                    hideProgress();
                    if (checkVerificationCodeResponse.isVerificates()) {
                        CovrEnterPersonalCodeFragment fragment = CovrEnterPersonalCodeFragment.getInstance(mPhoneNumber);
                        replaceFragment(fragment, fragment.getArguments(), true, FragmentAnimationSet.FADE_IN);
                    } else {
                        new Handler(Looper.getMainLooper()).post(() -> Toast.makeText(getActivity(), "Sms not verified", Toast.LENGTH_SHORT).show());
                    }
                }

                @Override
                public void onError(List<Error> errors) {
                    Log.e("CovrEnterSmsFragment", "" + errors);
                    hideProgress();
                    getActivity().runOnUiThread(() -> Toast.makeText(getActivity(), "" + errors, Toast.LENGTH_SHORT).show());
                }
            });
        });
    }
}