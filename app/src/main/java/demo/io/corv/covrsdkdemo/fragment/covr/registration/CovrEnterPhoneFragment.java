package demo.io.corv.covrsdkdemo.fragment.covr.registration;

import android.util.Log;
import android.view.LayoutInflater;
import android.widget.Toast;

import com.covrsecurity.io.network.base.IResponseListener;
import com.covrsecurity.io.network.response.Error;
import com.covrsecurity.io.sdk.model.request.SendVerificationCodeRequestSdk;

import java.util.List;

import demo.io.corv.covrsdkdemo.FragmentAnimationSet;
import demo.io.corv.covrsdkdemo.R;
import demo.io.corv.covrsdkdemo.SdkTestApp;
import demo.io.corv.covrsdkdemo.databinding.FragmentCovrEnterPhoneBinding;
import demo.io.corv.covrsdkdemo.fragment.BaseFragment;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class CovrEnterPhoneFragment extends BaseFragment<FragmentCovrEnterPhoneBinding> {

    private static final String REGISTERED_EXTRA = "REGISTERED_EXTRA";

    public static CovrEnterPhoneFragment getInstance() {
        return new CovrEnterPhoneFragment();
    }

    private Disposable mDisposable;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_covr_enter_phone;
    }

    @Override
    protected void initBinding(LayoutInflater inflater) {
        super.initBinding(inflater);
        mBinding.sendSms.setOnClickListener(v -> {
            showProgress();
            SendVerificationCodeRequestSdk request = new SendVerificationCodeRequestSdk(mBinding.phoneEdit.getText().toString());
            SdkTestApp.getsSdkTestApp().getRegistrationApiPublic().sendVerificationCode(request).enqueue(new IResponseListener<Void>() {
                @Override
                public void onResponseReceived(Void aVoid) {
                    Log.w("CovrEnterPhoneFragment", "" + aVoid);
                    hideProgress();
                    CovrEnterSmsFragment fragment = CovrEnterSmsFragment.getInstance(mBinding.phoneEdit.getText().toString());
                    replaceFragment(fragment, fragment.getArguments(), true, FragmentAnimationSet.FADE_IN);
                }

                @Override
                public void onError(List<Error> errors) {
                    Log.e("CovrEnterPhoneFragment", "" + errors);
                    hideProgress();
                    getActivity().runOnUiThread(() -> Toast.makeText(getActivity(), "" + errors, Toast.LENGTH_SHORT).show());
                }
            });
        });
        if (!SdkTestApp.getsSdkTestApp().getCovrSdkMainClass().isRegistrationHubInitialized()) {
            showProgress();
            mDisposable = Single.fromCallable(() -> SdkTestApp.getsSdkTestApp().getCovrSdkMainClass().initializeRegistrationHubSync())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnDispose(SdkTestApp.getsSdkTestApp().getCovrSdkMainClass()::cancelInitializeRegistrationHub)
                    .subscribe(s -> {
                        Log.w("CovrEnterPhoneFragment", s);
                        hideProgress();
                    }, throwable -> {
                        hideProgress();
                        Log.e("CovrEnterPhoneFragment", throwable.getMessage());
                        getActivity().runOnUiThread(() -> Toast.makeText(getActivity(), "" + throwable.getMessage(), Toast.LENGTH_SHORT).show());
                    });
        } else {
            Toast.makeText(getActivity(), "Do your staff", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mDisposable != null) {
            mDisposable.dispose();
        }
    }
}