package demo.io.corv.covrsdkdemo.activity;

import android.app.Fragment;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import demo.io.corv.covrsdkdemo.FragmentAnimationSet;
import demo.io.corv.covrsdkdemo.R;
import demo.io.corv.covrsdkdemo.SdkTestApp;
import demo.io.corv.covrsdkdemo.fragment.DefaultFragment;
import demo.io.corv.covrsdkdemo.fragment.covr.CovrLockScreenFragment;
import demo.io.corv.covrsdkdemo.fragment.covr.CovrMainScreenFragment;
import demo.io.corv.covrsdkdemo.fragment.covr.registration.CovrEnterPhoneFragment;

public class Main2Activity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private NavigationView mNavigationView;

    @Override
    protected int getFragmentContainerId() {
        return R.id.container;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> {
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        });

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        mNavigationView = findViewById(R.id.nav_view);
        mNavigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mNavigationView.setCheckedItem(R.id.nav_camera);
        DefaultFragment fragment = DefaultFragment.getInstance("nav_camera");
        replaceFragment(fragment, fragment.getArguments(), false);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        Fragment fragment;
        switch (item.getItemId()) {
            case R.id.nav_camera:
                fragment = DefaultFragment.getInstance("nav_camera");
                replaceFragment(fragment, fragment.getArguments(), false);
                break;
            case R.id.nav_gallery:
                fragment = DefaultFragment.getInstance("nav_gallery");
                replaceFragment(fragment, fragment.getArguments(), false);
                break;
            case R.id.nav_slideshow:
                fragment = DefaultFragment.getInstance("nav_slideshow");
                replaceFragment(fragment, fragment.getArguments(), false);
                break;
            case R.id.nav_covr:
                boolean registered = SdkTestApp.getsSdkTestApp().getCovrSdkMainClass().isRegistered();
                Log.w("Main2Activity", "registered = " + registered);
                if (!registered) {
                    fragment = CovrEnterPhoneFragment.getInstance();
                    replaceFragment(fragment, fragment.getArguments(), false);
                } else if (SdkTestApp.getsSdkTestApp().getCovrSdkMainClass().isNotificationHubInitialized()) {
                    fragment = CovrMainScreenFragment.getInstance();
                    replaceFragment(fragment, fragment.getArguments(), true, FragmentAnimationSet.FADE_IN);
                } else {
                    fragment = CovrLockScreenFragment.getInstance();
                    replaceFragment(fragment, fragment.getArguments(), false);
                }
                break;
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
