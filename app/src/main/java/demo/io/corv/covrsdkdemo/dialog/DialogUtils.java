package demo.io.corv.covrsdkdemo.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.text.TextUtils;

public final class DialogUtils {

    public static AlertDialog showOkDialog(Activity context, String title, String message,
                                           String positiveText, DialogInterface.OnClickListener onClickPositive,
                                           boolean cancelable) {
        if (!context.isFinishing()) {
            AlertDialog dialog = new AlertDialog.Builder(context).create();
            if (!TextUtils.isEmpty(title)) {
                dialog.setTitle(title);
            }
            if (!TextUtils.isEmpty(message)) {
                dialog.setMessage(message);
            }
            dialog.setButton(DialogInterface.BUTTON_POSITIVE,
                    !TextUtils.isEmpty(positiveText) ? positiveText : context.getString(android.R.string.ok), onClickPositive);
            dialog.setCancelable(cancelable);
            dialog.show();
            return dialog;
        }
        return null;
    }
}
