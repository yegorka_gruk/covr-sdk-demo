package demo.io.corv.covrsdkdemo;

import android.support.multidex.MultiDexApplication;

import com.covrsecurity.io.sdk.CovrSdkMainClass;
import com.covrsecurity.io.sdk.RegistrationApiPublic;

import java.io.File;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

import timber.log.Timber;

public class SdkTestApp extends MultiDexApplication {

    public static final String COVR_DIRECTORY_NAME = "CovrDirectory";

    private static SdkTestApp sSdkTestApp;

    public static SdkTestApp getsSdkTestApp() {
        return sSdkTestApp;
    }

    private CovrSdkMainClass mCovrSdkMainClass;

    public CovrSdkMainClass getCovrSdkMainClass() {
        return mCovrSdkMainClass;
    }

    public RegistrationApiPublic getRegistrationApiPublic() {
        return mCovrSdkMainClass.getRegistrationApiPublic();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sSdkTestApp = this;
        try {
            mCovrSdkMainClass = new CovrSdkMainClass(this, new File(getFilesDir().getAbsolutePath() + File.separator + COVR_DIRECTORY_NAME));
        } catch (InvalidAlgorithmParameterException | NoSuchAlgorithmException | NoSuchProviderException | KeyStoreException e) {
            Timber.e(e);
        }
    }
}
