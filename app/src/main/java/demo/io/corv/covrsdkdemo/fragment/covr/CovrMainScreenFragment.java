package demo.io.corv.covrsdkdemo.fragment.covr;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.Toast;

import com.covrsecurity.io.network.base.IResponseListener;
import com.covrsecurity.io.network.request.notification.GetConnectionsRequest;
import com.covrsecurity.io.network.request.notification.PostQrCodeRequest;
import com.covrsecurity.io.network.response.Error;
import com.covrsecurity.io.network.response.notification.GetConnectionsResponse;
import com.covrsecurity.io.network.response.notification.GetPushNotificationsResponse;
import com.covrsecurity.io.network.response.notification.Merchants;
import com.covrsecurity.io.network.response.notification.PostQrCodeResponse;
import com.covrsecurity.io.sdk.utils.ThreadUtils;

import java.util.List;

import demo.io.corv.covrsdkdemo.R;
import demo.io.corv.covrsdkdemo.SdkTestApp;
import demo.io.corv.covrsdkdemo.databinding.FragmentCovrMainScreenBinding;
import demo.io.corv.covrsdkdemo.dialog.DialogUtils;
import demo.io.corv.covrsdkdemo.dialog.QrDialogFragment;
import demo.io.corv.covrsdkdemo.fragment.BaseFragment;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class CovrMainScreenFragment extends BaseFragment<FragmentCovrMainScreenBinding> {

    public static final String DIALOG_QR_TAG = "DIALOG_QR_TAG";
    public static final int CAMERA_REQUEST_CODE = 1235;

    public static CovrMainScreenFragment getInstance() {
        return new CovrMainScreenFragment();
    }

    private Disposable mDisposable;

    @Override
    protected void initData(Bundle savedInstanceState) {
        super.initData(savedInstanceState);
    }

    @Override
    protected void initBinding(LayoutInflater inflater) {
        super.initBinding(inflater);
        mBinding.addMerchant.setOnClickListener(v -> {
            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                showQrDialogFragment();
            } else {
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, CAMERA_REQUEST_CODE);
            }
        });
        if (!SdkTestApp.getsSdkTestApp().getCovrSdkMainClass().isNotificationHubInitialized()) {
            createHubConnection();
        } else {
            Toast.makeText(getActivity(), "Do your staff", Toast.LENGTH_SHORT).show();
        }

    }

    private void createHubConnection() {
        showProgress();
        mDisposable = Single.fromCallable(() -> SdkTestApp.getsSdkTestApp().getCovrSdkMainClass().initializeNotificationHubSync())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
                            Log.w("CovrLockScreenFragment", "chechCovrCode: " + s);
                            Toast.makeText(getActivity(), "aBoolean " + s, Toast.LENGTH_SHORT).show();
                            hideProgress();
                            loadMerchants();
                        },
                        throwable -> {
                            Log.e("CovrLockScreenFragment", "chechCovrCode: " + throwable.getMessage());
                            Toast.makeText(getActivity(), "chechCovrCode " + throwable.getMessage(), Toast.LENGTH_SHORT).show();
                            hideProgress();
                            DialogUtils.showOkDialog(
                                    getActivity(),
                                    getString(R.string.error_no_internet_title),
                                    getString(R.string.error_login_generic),
                                    getString(R.string.retry),
                                    (dialog, which) -> createHubConnection(),
                                    false);
                        });
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_covr_main_screen;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == CAMERA_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                showQrDialogFragment();
            } else {
                Toast.makeText(getActivity(), R.string.camera_permission_declined, Toast.LENGTH_SHORT).show();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mDisposable != null) {
            mDisposable.dispose();
        }
    }

    private void showQrDialogFragment() {
        QrDialogFragment dialogFragment = QrDialogFragment.getInstance();
        dialogFragment.setScanListener((text, points) -> {
            dialogFragment.dismiss();
            connectMerchant(text);
            Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();
        });
        dialogFragment.show(getChildFragmentManager(), DIALOG_QR_TAG);
    }

    private void connectMerchant(String qrCodeString) {
        showProgress();
        SdkTestApp.getsSdkTestApp().getCovrSdkMainClass().getNotificationApi()
                .postQrCode(new PostQrCodeRequest(qrCodeString))
                .enqueue(new IResponseListener<PostQrCodeResponse>() {
                    @Override
                    public void onResponseReceived(PostQrCodeResponse response) {
                        Log.w("CovrLockScreenFragment", "consumerMerchantQRCode: " + response);
                        ThreadUtils.scheduleOnMainThread(() -> Toast.makeText(getActivity(), "consumerMerchantQRCode " + response, Toast.LENGTH_SHORT).show());
                        loadMerchants();
                        subscribeToGetMessages();
                        hideProgress();
                    }

                    @Override
                    public void onError(List<Error> errors) {
                        Log.e("CovrLockScreenFragment", "consumerMerchantQRCode: " + errors);
                        ThreadUtils.scheduleOnMainThread(() -> Toast.makeText(getActivity(), "consumerMerchantQRCode " + errors, Toast.LENGTH_SHORT).show());
                        hideProgress();
                    }
                });
    }

    private void loadMerchants() {
        GetConnectionsRequest request = new GetConnectionsRequest(0, 10);
        SdkTestApp.getsSdkTestApp().getCovrSdkMainClass().getNotificationApi()
                .getConnections(request)
                .enqueue(new IResponseListener<GetConnectionsResponse>() {
                    @Override
                    public void onResponseReceived(GetConnectionsResponse response) {
                        Log.w("CovrLockScreenFragment", "consumerMerchantsGet: " + response);
                        String text = "";
                        List<Merchants> merchants = response.getMerchants();
                        for (Merchants merchant : merchants) {
                            text += merchant.getMerchant().getName() + "\n";
                        }
                        String finalText = text;
                        ThreadUtils.scheduleOnMainThread(() -> {
                            mBinding.merchants.setText("Merchants : " + finalText);
                        });
                    }

                    @Override
                    public void onError(List<Error> errors) {
                        Log.e("CovrLockScreenFragment", "consumerMerchantsGet: " + errors);
                        ThreadUtils.scheduleOnMainThread(() -> Toast.makeText(getActivity(), "consumerMerchantsGet " + errors, Toast.LENGTH_SHORT).show());
                    }
                });
    }

    private void subscribeToGetMessages() {
        SdkTestApp.getsSdkTestApp().getCovrSdkMainClass().getNotificationApi()
                .getPushNotifications()
                .subscribeToEvent(new IResponseListener<GetPushNotificationsResponse>() {
                    @Override
                    public void onResponseReceived(GetPushNotificationsResponse response) {
                        Log.w("CovrLockScreenFragment", "getMessages: " + response);
                    }

                    @Override
                    public void onError(List<Error> errors) {
                        Log.e("CovrLockScreenFragment", "getMessages: " + errors);
                    }
                });
    }
}
